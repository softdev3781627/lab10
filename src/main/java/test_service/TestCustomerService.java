/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package test_service;

import com.werapan.databaseproject.model.Customer;
import com.werapan.databaseproject.service.CustomerService;

/**
 *
 * @author kunpo
 */
public class TestCustomerService {
    public static void main(String[] args) {
        CustomerService cs = new CustomerService();
        for(Customer customer:cs.getCustomers()){
            System.out.println(customer);
        }
        System.out.println(cs.getByTel("0888888898"));
        Customer cus1 = new Customer("kew","0999999999");
        cs.addNew(cus1);
        for(Customer customer:cs.getCustomers()){
            System.out.println(customer);
        }
        Customer delcus = cs.getByTel("0999999999");
        delcus.setTel("0999999991");
        cs.update(delcus);
        System.out.println("After Update");
        for(Customer customer:cs.getCustomers()){
            System.out.println(customer);
        }
        cs.delete(delcus);
        for(Customer customer:cs.getCustomers()){
            System.out.println(customer);
        }
    }
}
